<?php
/*
 * Copyright notice
 *
 * (c) 2023 touch5  (info@touch5.de)
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 */
namespace Touch5\T5BootstrapForm\Domain\Finishers;

use TYPO3\CMS\Core\Mail\FluidEmail;
use TYPO3\CMS\Form\Domain\Runtime\FormRuntime;
use TYPO3\CMS\Form\Domain\Finishers\EmailFinisher;

class EmailFinisherBootstrap extends TYPO3\CMS\Form\Domain\Finishers\EmailFinisher
{
    protected function initializeFluidEmail(FormRuntime $formRuntime): FluidEmail
    {
        $fluidEmail = parent::initializeFluidEmail($formRuntime);

        $bodytext = $this->parseOption('bodytext');
        $image = $this->parseOption('image');

        if ($image) {
            $fluidEmail->assign('image', $image);
        }
        if ($bodytext) {
            $fluidEmail->assign('bodytext', $bodytext);
        }
        return $fluidEmail;
    }
}
