<?php

declare(strict_types=1);

namespace Touch5\T5BootstrapForm\Tests\Unit\Domain\Model;

use PHPUnit\Framework\MockObject\MockObject;
use TYPO3\TestingFramework\Core\AccessibleObjectInterface;
use TYPO3\TestingFramework\Core\Unit\UnitTestCase;

/**
 * Test case
 *
 * @author Michael Scheunemann <m.scheunemann@touch5.de>
 */
class VariableTest extends UnitTestCase
{
    /**
     * @var \Touch5\T5BootstrapForm\Domain\Model\Variable|MockObject|AccessibleObjectInterface
     */
    protected $subject;

    protected function setUp(): void
    {
        parent::setUp();

        $this->subject = $this->getAccessibleMock(
            \Touch5\T5BootstrapForm\Domain\Model\Variable::class,
            ['dummy']
        );
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getFooterTextReturnsInitialValueForMixed(): void
    {
    }

    /**
     * @test
     */
    public function setFooterTextForMixedSetsFooterText(): void
    {
    }
}
