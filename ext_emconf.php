<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 't5_bootstrap_form',
    'description' => 'Extendet form structur for bootstrap',
    'category' => 'plugin',
    'author' => 'Michael Scheunemann',
    'author_email' => 'm.scheunemann@touch5.de',
    'state' => 'stable',
    'clearCacheOnLoad' => 0,
    'version' => '4.13.0',
    'constraints' => [
        'depends' => [
            'typo3' => '^12.5.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
